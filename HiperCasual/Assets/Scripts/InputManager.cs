﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    private static InputManager _instance;
    public static InputManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = (InputManager)GameObject.FindObjectOfType(typeof(InputManager));
                if (_instance == null)
                {
                    GameObject ic = new GameObject("Input Manager");
                    _instance = ic.AddComponent<InputManager>();
                }
            }

            return _instance;
        }
    }

    public delegate void StartedTouch();
    public static event StartedTouch OnStartTouch;
    public delegate void EndedTouch();
    public static event StartedTouch OnEndTouch;

    public bool Fire1Down = false;
    public bool Fire1 = false;
    public Touch FirstTouch;
    public bool IsTouchingScreen = false;

    private void Update() {
        Fire1Down = Input.GetButtonDown("Fire1");
        Fire1 = Input.GetButton("Fire1");
        if (Input.touchCount > 0)
        {
            if (!IsTouchingScreen && OnStartTouch != null)
                OnStartTouch();
            
            FirstTouch = Input.GetTouch(0);
            IsTouchingScreen = true;
        }
        else
        {
            if (IsTouchingScreen && OnEndTouch != null)
                OnEndTouch();

            IsTouchingScreen = false;            
        }
    }

    void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }
}
