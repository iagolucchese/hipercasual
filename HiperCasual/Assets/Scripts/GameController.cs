﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    private static GameController _instance;
	public static GameController Instance
    {
        get
        {
            if (_instance == null)
            {
                Debug.LogError("No GameController instance.");
            }
            return _instance;
        }
    }
    [Header("Prefabs")]
    public GameObject linePrefab;

    [Header("Speeds")]
    public float StartSpeed = 1f;
    public float MaxSpeed = 10f;
    public float CurrentSpeed;

    [Header("Difficulty")]
    public int StartingBlocks = 6;
    public int CurrentBlocks;
    public int LineCount = 0;

    private InputManager input;

    private void Start() 
    {
        input = InputManager.Instance;

        CurrentSpeed = StartSpeed;
        CurrentBlocks = StartingBlocks;   

        //start the game by making a new line
    }

    void Update()
    {
        if (input.Fire1Down && CurrentBlocks > 0)
        {
            
        }
    }

    private void StartGame()
    {
        MakeNewLine(CurrentBlocks, CurrentSpeed);
    }

    private void MakeNewLine(int size, float speed)
    {
        var newLine = Instantiate(linePrefab, new Vector3(0f, LineCount, 0f), Quaternion.identity);            
        var lb = newLine.GetComponent<LineBehavior>();
        lb.CreateLineOfBlocks(CurrentBlocks);

        LineCount++;
    }

    void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }
}
