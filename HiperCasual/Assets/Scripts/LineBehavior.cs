﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineBehavior : MonoBehaviour
{
    public GameObject blockPrefab;
    public List<GameObject> listOfBlocks;
    public int directionOfMovement = 0; // -1 pra ir pra esquerda, 1 pra direita, 0 pra ficar parado
    public float delayBetweenMoves;
    public int lineSize = 0;

    private float delayCounter = 0f;

    void Start()
    {
        
    }

    public void CreateLineOfBlocks(int size)
    {
        listOfBlocks = new List<GameObject>();
        lineSize = size;
        for (int i = 0; i < size; i++)
        {
            var newBlock = Instantiate(blockPrefab, new Vector3(i - (size-1)*0.5f, transform.position.y, 0f), Quaternion.identity);
            newBlock.transform.parent = this.transform;
            newBlock.name = "Block " + i;
            listOfBlocks.Add(newBlock);
        }
    }

    void Update()
    {
        if (delayCounter <= 0f)
        {
            if (directionOfMovement < 0)
            {

            }
            else if (directionOfMovement > 0)
            {

            }
            else
            {
                
            }
        }
        else
        {
            delayCounter -= Time.deltaTime;
        }
    }
}
